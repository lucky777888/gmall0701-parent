package com.atguigu.gmall.realtime.common;

/**
 * Author: Felix
 * Date: 2021/12/26
 * Desc: 项目配置常量类
 */
public class GmallConfig {
    public static final String HBASE_SCHEMA="GMALL0701_REALTIME";
    public static final String PHOENIX_SERVER="jdbc:phoenix:hadoop202,hadoop203,hadoop204:2181";
}

