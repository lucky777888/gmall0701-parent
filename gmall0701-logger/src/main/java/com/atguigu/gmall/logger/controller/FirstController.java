package com.atguigu.gmall.logger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author: Felix
 * Date: 2021/12/22
 * Desc: 第一个SpringMVC程序
 * @Controller 将对象的创建以及对象之间关系的维护交给Spring容器进行管理
 *      如果类中的方法返回的是String，那么会进行页面的跳转；如果不想跳转，只是返回String，需要
 *      在方法上加一个注解
 *
 * @RestController = @Controller + @ResponseBody
 * @RequestMapping  拦截请求，交给对应的方法进行处理
 */
@RestController
public class FirstController {
    @RequestMapping("/login")
    public String first(@RequestParam("hehe") String username,
                        @RequestParam(value="haha",defaultValue = "atguigu") String password){
        System.out.println("username:" + username + ",password:" + password);
        return "success";
    }
}
