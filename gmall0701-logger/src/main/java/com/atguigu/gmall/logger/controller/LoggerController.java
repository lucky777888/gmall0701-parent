package com.atguigu.gmall.logger.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Properties;

/**
 * Author: Felix
 * Date: 2021/12/22
 * Desc: 日志数据的采集处理服务
 */
@RestController
@Slf4j
public class LoggerController {
    //private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LoggerController.class);


    @Autowired
    private KafkaTemplate kafkaTemplate;

    @RequestMapping("/applog")
    public String logger(@RequestParam("param") String logStr){
        //打印输出
        //System.out.println(logStr);

        //落盘  IO   使用第三方记录日志的插件logback
        log.info(logStr);

        //发送到kafka主题
        kafkaTemplate.send("ods_base_log",logStr);

        return "success";
    }
}
