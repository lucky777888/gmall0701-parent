package com.atguigu.cdc;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.ververica.cdc.connectors.mysql.MySQLSource;
import com.alibaba.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.alibaba.ververica.cdc.debezium.DebeziumDeserializationSchema;
import com.alibaba.ververica.cdc.debezium.DebeziumSourceFunction;
import com.alibaba.ververica.cdc.debezium.StringDebeziumDeserializationSchema;
import io.debezium.data.Envelope;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.source.SourceRecord;

import java.util.List;

/**
 * Author: Felix
 * Date: 2021/12/25
 * Desc: 使用FlinkCDC读取Mysql数据  DataStreamAPI方式
 */
public class FlinkCDC03_Custom {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DebeziumSourceFunction<String> sourceFunction = MySQLSource.<String>builder()
            .hostname("hadoop202")
            .port(3306)
            .databaseList("gmall0701_realtime")
            .tableList("gmall0701_realtime.t_user")
            .username("root")
            .password("123456")
            .deserializer(new MyDebeziumDeserializationSchema())
            .startupOptions(StartupOptions.initial())
            .build();

        env.addSource(sourceFunction).print(">>>");

        env.execute("Print MySQL Snapshot + Binlog");
    }
}

class MyDebeziumDeserializationSchema implements DebeziumDeserializationSchema<String> {

    /**
     *
     ConnectRecord{
        value=Struct{
            after=Struct{id=4,name=aa,age=22},
            source=Struct{db=gmall0701_realtime,table=t_user},
            op=c
        }
     }

     {"id":4,"name":"aa","age":22}
     */
    @Override
    public void deserialize(SourceRecord sourceRecord, Collector<String> out) throws Exception {
        Struct valueStruct = (Struct)sourceRecord.value();
        Struct afterStruct = valueStruct.getStruct("after");
        Struct sourceStruct = valueStruct.getStruct("source");

        //获取数据库名称
        String database = sourceStruct.getString("db");
        //获取表名
        String table = sourceStruct.getString("table");

        String type = Envelope.operationFor(sourceRecord).toString().toLowerCase();

        if("create".equals(type)){
            type = "insert";
        }

        //创建一个json对象，用于封装返回的信息
        JSONObject resJsonObj = new JSONObject();
        resJsonObj.put("database",database);
        resJsonObj.put("table",table);
        resJsonObj.put("type",type);

        //获取影响的记录
        JSONObject dataJsonObj = new JSONObject();
        if(afterStruct != null){
            List<Field> fieldList = afterStruct.schema().fields();
            for (Field field : fieldList) {
                String fieldName = field.name();
                Object fieldValue = afterStruct.get(field);
                dataJsonObj.put(fieldName,fieldValue);
            }
        }
        resJsonObj.put("data",dataJsonObj);

        out.collect(resJsonObj.toJSONString());

    }

    @Override
    public TypeInformation<String> getProducedType() {
        return TypeInformation.of(String.class);
    }
}
